# **v-drive**

## Overview

v-drive is a tool for the game ████████ written in C++ and compiled as a library (.so).

This tool provides a set of functionalities for game interaction  and gameplay. This public release is only a demo made for this public repo, the automated gameplay and path selection is not available. 
This project's features violate all the game policies, I am not responsible for any repercussions caused by the use of this software, it has been made only as a development "workout".

## Features

- **Target Selection**: Finds the best target, aims and shoots at their head.

## How to Use

The original projects is a child library of Pandora, a management tool with a SDK that is not available publicly.
Every function inside the kernel class should be replaced by your own implementation, everything else works.


[Watch Demo Video](assets/demo.mp4)