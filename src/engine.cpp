#include "headers/pch.h"

using namespace Engine;

QWORD Cached::world = 0;
QWORD Cached::xor_keys = 0;
QWORD Cached::game_state = 0;
TArray<QWORD> Cached::player_state_array = {};
CameraComponent* Cached::camera_component = 0;
LocalPlayer Cached::me = LocalPlayer();
std::vector<Entity> Cached::entities = {};

const char * Engine::GetName(int id)
{
    if(!Cached::xor_keys) return " ";

    DWORD chunk_offset = (DWORD)((int)(id) >> 16);
    WORD name_offset = (WORD)id;

    QWORD name_pool_chunk = Read<QWORD>(Memory::base_address + Offsets::FNamePool + ((chunk_offset +2)*8));
    if(!name_pool_chunk)
    {
        print("WARNING - Invalid name_pool_chunk");
        return " ";
    }
    
    QWORD entry_offset = name_pool_chunk + (QWORD)(name_offset *4);

    FNameEntry name_entry = Read<FNameEntry>(entry_offset);
    if(!name_entry.comparison_id)
    {
        print("WARNING - Invalid name_entry");
        return " ";
    }

    char* name = name_entry.ansi_name;

    for(WORD i= 0; i < name_entry.header.len; i++)
        name[i] ^= name_entry.header.len ^ *((LPBYTE)&Cached::xor_keys + (i & 3));

    name[name_entry.header.len] =  '\0';
    return name;
}


bool Engine::Update()
{
    QWORD world = 0;
    if(!kernel->ReadKernel(Memory::guarded_pool_address + 0x60, (QWORD)&world, sizeof(world))) return true;
    //print("world: %p", world);

    if(!Cached::xor_keys)
        if(!kernel->ReadKernel(Memory::guarded_pool_address + 0x0, (QWORD)&Cached::xor_keys, sizeof(Cached::xor_keys))) return true;

    
    QWORD game_state = Read<QWORD>(world + Offsets::GameState);
    if(!game_state) return true;
    //print("game_state: %p",game_state);

    TArray<QWORD> array = Read<TArray<QWORD>>(game_state + Offsets::NetworkablePlayerStateTArray);
    if(!array.data or !array.count) return true;
    //print("array.data: %p, count: %i",array.data, array.count);

    if(game_state == Cached::game_state)
        if(array == Cached::player_state_array)
            return false;
            
    //print("New game detected, caching...");

    QWORD game_instance = Read<QWORD>(world + Offsets::GameInstance);
    if(!game_instance) return true;
    //print("game_instance: %p",game_instance);

    QWORD local_player_array = Read<QWORD>(game_instance + Offsets::LocalPlayerArray);
    if(!local_player_array) return true;
    //print("local_player_array: %p",local_player_array);

    QWORD local_player = Read<QWORD>(local_player_array + Offsets::LocalPlayer);
    if(!local_player) return true;
    //print("local_player: %p",local_player);

    QWORD local_player_controller = Read<QWORD>(local_player + Offsets::LocalPlayerController);
    if(!local_player_controller) return true;
    //print("local_player_controller: %p",local_player_controller);

    QWORD local_player_pawn = Read<QWORD>(local_player_controller + Offsets::LocalPlayerPawn);
    if(!local_player_pawn) return true;
    //print("local_player_pawn: %p",local_player_pawn);
    
    QWORD local_damage_component = Read<QWORD>(local_player_pawn + Offsets::DamageComponent);
    if(!local_damage_component) return true;
    //print("local_damage_component: %p",local_damage_component);

    float local_health = Read<float>(local_damage_component + Offsets::Health);
    //print("local_health: %f",local_health);

    if(local_health <= 0.f or local_health > 100.f) return true;

    QWORD local_root_component = Read<QWORD>(local_player_pawn + Offsets::RootComponent);
    if(!local_root_component) return true;
    //print("local_root_component: %p",local_root_component);

    QWORD local_player_state = Read<QWORD>(local_player_pawn + Offsets::PlayerState);
    if(!local_player_state) return true;
    //print("local_player_state: %p",local_player_state);

    QWORD local_team_component = Read<QWORD>(local_player_state + Offsets::TeamComponent);
    if(!local_team_component) return true;
    //print("local_team_component: %p",local_team_component);

    TEAMID local_team = Read<TEAMID>(local_team_component + Offsets::TeamID);
    //print("local_team: %i",local_team);

    QWORD local_inventory = Read<QWORD>(local_player_pawn + Offsets::InventoryComponent);
    if(!local_inventory) return true;
    //print("local_inventory: %p",local_inventory);

    // auto my_inv = (InventoryComponent*)local_inventory;
    // auto current = my_inv->GetCurrentEquipped();
    // auto c_id = Read<int>((QWORD)current + 0x18);
    // auto c_name = GetName(c_id);
    //print("%s", c_name);
    // return true;

    QWORD camera_component = Read<QWORD>(local_player_controller + Offsets::CameraComponent);
    if(!camera_component) return true;
    //print("camera_component: %p",camera_component);
    
    // auto rot = ((CameraComponent*)camera_component)->GetInfo().Rotation;
    //print("%f - %f - %f", rot.Pitch, rot.Yaw, rot.Roll);

    std::vector<Entity> local_collection;

    for(int i = 0; i < array.count; i ++)
    {
        redo:

        auto entry = array[i];
        //print("entry: %p", entry);
        if(!entry) continue;

        if(entry == local_player_state) continue;

        auto team_component = Read<QWORD>(entry + Offsets::TeamComponent);
        //print("team_component: %p", team_component);
        if(!team_component) goto redo;

        auto team = Read<TEAMID>(team_component + Offsets::TeamID);
        //print("team: %i", team);
        if(team == local_team) continue;

        auto pawn = Read<QWORD>(entry + Offsets::PawnSpawnedCharacter);
        //print("pawn: %p", pawn);
        if(!pawn) goto redo;

        auto damage = Read<QWORD>(pawn + Offsets::DamageComponent);
        //print("damage: %p", damage);
        if(!damage) goto redo;

        auto root = Read<QWORD>(pawn + Offsets::RootComponent);
        //print("root: %p", root);
        if(!root) goto redo;

        auto mesh = Read<QWORD>(pawn + Offsets::MeshComponent);
        //print("mesh: %p", mesh);
        if(!mesh) goto redo;

        auto bone_array = Read<TArray<FTransform>>(mesh + Offsets::BoneTArray);
        if(!bone_array.data) goto redo;
        //print("count: %i", bone_array.count);

        Entity ent = Entity(pawn, damage, root, mesh, bone_array);
        local_collection.push_back(ent);
    }

    //if(local_collection.empty()) return true;
    print("new game detected: n enemies: %i", local_collection.size());

    Cached::world = world;
    Cached::game_state = game_state;
    Cached::player_state_array = array;
    Cached::camera_component = (CameraComponent*)camera_component;
    Cached::me = LocalPlayer(local_player_pawn, local_damage_component, local_player_controller, local_inventory);
    Cached::entities = local_collection;

    return false;
}
