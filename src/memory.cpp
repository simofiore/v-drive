#include "headers/pch.h"

QWORD Memory::base_address = 0;
QWORD Memory::guarded_pool_address = 0;

bool Memory::ReadEx(QWORD address, QWORD buffer, size_t size)
{
    bool result = false;

    DWORD rsh = address >> 0x24;
    if(rsh == 0x8 || rsh == 0x10)
    {
        result = kernel->ReadKernel(guarded_pool_address + (address & 0xffffff), buffer, size);
    }
    else
    {
        result = kernel->Read(address, buffer, size);
    }
    return result;
}
