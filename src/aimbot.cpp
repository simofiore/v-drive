#include "headers/pch.h"
Entity* Aimbot::_last_target = nullptr;
QWORD Aimbot::_last_target_time = 0;

using namespace Engine;
#pragma region Helpers

Vector3 CalculateAngle(Vector3& Src, Vector3& Dst) {
	Vector3 Delta = Src - Dst;

	float hyp = sqrt(Delta.x * Delta.x + Delta.y * Delta.y + Delta.z * Delta.z);

	Vector3 Rotation{};
	Rotation.x = RadianToDegree(acosf(Delta.z / hyp));
	Rotation.y = RadianToDegree(atanf(Delta.y / Delta.x));
	Rotation.z = 0;
	if (Delta.x >= 0.0f) Rotation.y += 180.0f;
	Rotation.x += 270.f;
	return Rotation;
}

void Clamp(Vector3& Ang) {
	if (Ang.x < 0.f)
		Ang.x += 360.f;

	if (Ang.x > 360.f)
		Ang.x -= 360.f;

	if (Ang.y < 0.f) 
		Ang.y += 360.f;

	if (Ang.y > 360.f) 
		Ang.y -= 360.f;

	Ang.z = 0.f;
}

void Normalize(Vector3& in)
{
	if (in.x > 89.f) in.x -= 360.f;
	else if (in.x < -89.f) in.x += 360.f;

	while (in.y > 180)in.y -= 360;
	while (in.y < -180)in.y += 360;
	in.z = 0;
}

Vector3 SmoothAim(Vector3& target, Vector3& delta_rotation, float smooth)
{
	Vector3 diff = target - delta_rotation;
	Normalize(diff);
	return delta_rotation + diff / smooth;
}

float CalculateDistanceBetweenAngles(Vector3& from, Vector3& to)
{
    auto diff_a = from - to;
    Normalize(diff_a);
    return diff_a.Length();
}

#pragma endregion

void InterpMouseMove(int x, int y)
{
    while (x != 0 or y != 0)
    {
        int local_x =0, local_y=0;
        if(x!=0)
        {
            local_x = std::min(std::max(x,-127),127);
            x-=local_x;
        }
        if(y!=0)
        {
            local_y = std::min(std::max(y,-127),127);
            y-= local_y;
        }
        kernel->SendMouseMove(local_x, local_y,0);
    }
}

Vector3 SmoothTo(Vector3 target, Vector3 source, float smooth)
{
    auto diff = target - source;
    Normalize(diff);
    return source + diff /smooth;
}

float calculate_slope(float sens, float fov)
{
    // static const float m = 0.000564;
    // static const float c = 0.008768;

    // return m * fov + c;
    static const float fov_c = 103;
    static const float angle_step_c = 0.071411;
    
    return (angle_step_c * fov / fov_c) / sens;
}

bool can_flick = true;

bool Flick(KillPacket* packet, float smoothness_unused = 1)
{

    Vector3 cam = Vector3(packet->camera_info.Rotation.Pitch,packet->camera_info.Rotation.Yaw, packet->camera_info.Rotation.Roll);
    
    float angle_step = calculate_slope(0.3, packet->camera_info.FOV);
    float angle_step_smooth = angle_step / 10.f; 

    Vector3 target;
    float distance_to_crosshair;
    float target_radius;
    float smooth;
    int fov = (int)packet->camera_info.FOV;
    if(fov == 21 or fov == 29 or fov == 41)
    {
        auto chest_pos = packet->target->GetMeshWrapper()->GetBonePosition(MeshWrapper::BONEID::CHEST);
        target = CalculateAngle(packet->camera_info.Location, chest_pos);         
        distance_to_crosshair = CalculateDistanceBetweenAngles(packet->local_rotation, target);
        target_radius = packet->head_radius * 1.75;
        smooth = 10.f;
        angle_step -= angle_step_smooth * 8.5f;
    }
    else
    {
        target = packet->head_angle;
        distance_to_crosshair = packet->distance_to_crosshair;
        target_radius = packet->head_radius;
        smooth = 10.f;
        angle_step -= angle_step_smooth * 8.f;
    }

    Clamp(target);

    auto smoothed = SmoothAim(cam,target,smooth);
    Clamp(smoothed);

    auto diff = smoothed - packet->local_rotation;
    Normalize(diff);

    //print("SLOPE: %f", angle_step);
    auto direction_y = ((diff.x ) / (-angle_step)); //0.071411
    auto direction_x = ((diff.y ) / (angle_step));

    kernel->SetRawInput(false);
    //print("%f - %f", packet->distance_to_crosshair , packet->head_radius);
    if(distance_to_crosshair < target_radius)
    {
        kernel->SendMouseInput(ARDUINO(LBUTTON), false);
        kernel->SendMouseInput(ARDUINO(LBUTTON), true);
        kernel->SetRawInput(true);
        return true;
    }
    else
    {
        InterpMouseMove(direction_x, direction_y);
        return false;
    }
    
}

void MouseRCSAngle(KillPacket* packet) {


    Vector3 control_rotation = packet->local_rotation; 
    Vector3 camera_rotation = Vector3(packet->camera_info.Rotation.Pitch, packet->camera_info.Rotation.Yaw, packet->camera_info.Rotation.Roll);
    Vector3 delta = camera_rotation - control_rotation;
    Normalize(delta);

    Vector3 target = packet->head_angle;
    Clamp(target);

    Vector3 new_rot = target - delta - delta; //		Vector3 new_rot = target - delta - delta;

    auto smoothed = SmoothAim(new_rot,control_rotation,10.f);
    Clamp(smoothed);

    auto diff = smoothed - control_rotation;
    Normalize(diff);

    float angle_step = calculate_slope(0.3, packet->camera_info.FOV);
    float angle_step_smooth = angle_step / 10.f; 
    angle_step -= angle_step_smooth * 7.f;

    auto direction_y = ((diff.x ) / (-angle_step)); //0.071411
    auto direction_x = ((diff.y ) / (angle_step));

    InterpMouseMove(direction_x, direction_y);

}

bool Aimbot::Kill(KillPacket* packet)
{
    auto root = packet->target->GetRootComponent();
    auto velocity = root->GetVelocity();
    if(abs(velocity.x) > 800 || abs(velocity.y) > 800) return false;

    // auto my_weapon = Cached::me.GetInventoryComponent()->GetCurrentEquipped();
    // if(!my_weapon->CanShoot()) return false;

    if(kernel->IsKeyPressed(MOUSE5))
    {
        if(kernel->IsKeyPressed(LBUTTON))
        {
            can_flick = false;
            MouseRCSAngle(packet);
        }
        else
        {
        if(can_flick)
            if(Flick(packet))
                can_flick = false;
        }
    }

    return true;
}


bool Aimbot::Update()
{

    // static bool can = true;
    // if(kernel->IsKeyPressed(MOUSE5))
    // {
    //         if(can)
    //         {
    //             auto rot = Cached::me.GetControllerComponent()->GetRotation();
    //             print("ROTATION - X: %f, Y: %f, Z: %f", rot.x, rot.y, rot.z);
    //             kernel->Sleep(500);
    //             kernel->SendMouseMove(1,1,0);
    //             kernel->Sleep(500);
    //             rot = Cached::me.GetControllerComponent()->GetRotation();
    //             auto camera = (CameraComponent*)Read<QWORD>((QWORD)Cached::me.GetControllerComponent() + Offsets::CameraComponent);
    //             print("ROTATION - X: %f, Y: %f, Z: %f", rot.x, rot.y, rot.z);
    //             auto fov = camera->GetInfo().FOV;
    //             print("FOV: %f", fov);
    //             print("EXPECTED: %f", calculate_slope(0.3, fov));
    //             can = false;
    //             //kernel->SendMouseMove(127,127,0);
    //         }
    // }
    // else
    // {
    //     can = true;
    // }
    // return true;
    
    // auto item = Cached::me.GetInventoryComponent()->GetCurrentEquipped()->GetState();
    // print("STATE; %i", item);
    // return true;
    if(!kernel->IsKeyPressed(MOUSE5))
    {
        can_flick = true;
        kernel->SetRawInput(true);
    }

    if(!Cached::me.IsValid() || !Cached::me.GetDamageComponent()->IsAlive()) return true;

    if(Cached::entities.empty()) return true;

    KillPacket packet = {};

    auto camera_component = (CameraComponent*)Read<QWORD>((QWORD)Cached::me.GetControllerComponent() + Offsets::CameraComponent);

    packet.camera_info = camera_component->GetInfo();
    packet.local_rotation = Cached::me.GetControllerComponent()->GetRotation();
    // auto me_mash = (MeshComponent*)Read<QWORD>((QWORD)Cached::me.GetPawn() + Offsets::MeshComponent);
    // auto me_head = me_mash->GetBonePosition(MeshComponent::BONEID::HEAD);
    //print("%f - %f - %f", packet.local_rotation.x, packet.local_rotation.y, packet.local_rotation.z);
    // packet.camera_info.Rotation.Pitch, packet.camera_info.Rotation.Yaw, packet.camera_info.Rotation.Roll,
    // packet.camera_info.FOV);

    for(auto &entry : Cached::entities)
    {
        if(!entry.IsValid()) continue;
        if(entry.GetPawn()->IsDormant()) continue;
        if(!entry.GetDamageComponent()->IsAlive()) continue;
        if(!entry.GetMeshWrapper()->GetMeshComponent()->IsRendered()) continue;

        auto head = entry.GetMeshWrapper()->GetBonePosition(MeshWrapper::BONEID::HEAD);
        auto head_top = Vector3(head.x, head.y, head.z + 8.f);

        auto head_angle = CalculateAngle(packet.camera_info.Location, head);
        //print("%f - %f - %f", head.y, head.x, head.z);

        auto head_top_angle = CalculateAngle(packet.camera_info.Location, head_top);

        float distance_to_crosshair = CalculateDistanceBetweenAngles(packet.local_rotation, head_angle);
        float distance_from_top = CalculateDistanceBetweenAngles(head_top_angle, head_angle);
        //print("%f", distance_from_top);

        if(distance_to_crosshair < packet.distance_to_crosshair)
        {
            packet.target = &entry;
            packet.head_angle = head_angle;
            packet.head_radius = distance_from_top;
            packet.distance_to_crosshair = distance_to_crosshair;
        }

    }

    //print("IM HERE");
    if(!packet.target) return false;

    //print("valid target :)");
    //da fare target switch
    Kill(&packet);
    return false;
}