#include "headers/pch.h"

extern "C" void* Entry(Kernel* k)
{
    if(cheat)
    {
        delete cheat;
        cheat = nullptr;
    }

    kernel = k;
    cheat = new Cheat();
    return cheat;
}

extern "C" void Exit()
{
    if(cheat)
    {
        delete cheat;
        cheat = nullptr;
    }
}