#include "headers/pch.h"

bool PawnComponent::IsDormant()
{
    return Read<int>((QWORD)this + Offsets::Dormant) == 1;
}

Vector3 ControllerComponent::GetRotation()
{
    return Read<Vector3>((QWORD)this + Offsets::ControlRotation);
}

Vector3 RootComponent::GetVelocity()
{
    return Read<Vector3>((QWORD)this + Offsets::RootVelocity);
}

Vector3 RootComponent::GetPosition()
{
    return Read<Vector3>((QWORD)this + Offsets::RootPosition);
}

// TArray<FTransform> MeshComponent::GetBoneArray()
// {
    
//     return Read<TArray<FTransform>>((QWORD)this + Offsets::BoneTArray);
// }

bool MeshComponent::IsRendered()
{
    float l_r_t = Read<float>((QWORD)this + Offsets::LastRenderTime);
    float l_s_t = Read<float>((QWORD)this + Offsets::LastSubmitTime);
    
    bool is_visible = l_r_t + 0.06f >= l_s_t;
    return is_visible;
}

Vector3 MeshWrapper::GetBonePosition(MeshWrapper::BONEID id)
{
    auto bone = _bone_array[id];
    auto component_to_world = Read<FTransform>((QWORD)_mesh + Offsets::ComponentToWorld);
    MATRIX mtx = bone.ToMatrixWithScale().Multiply(component_to_world.ToMatrixWithScale());
    return Vector3(mtx._41, mtx._42, mtx._43);
}

Equipped::STATE Equipped::GetState()
{
    return Read<STATE>((QWORD)this + Offsets::CurrentEquippedVFXState);
}

bool Equipped::CanShoot()
{
    auto state = GetState();
    if(state == STATE::UNKNOWN || state == STATE::IDLE || state == STATE::FIRING) return true;
    else return false;
}

Equipped* InventoryComponent::GetCurrentEquipped()
{
    return Read<Equipped*>((QWORD)this + Offsets::CurrentEquipped);
}

float DamageComponent::GetHealth()
{
    return Read<float>((QWORD)this + Offsets::Health);
}

bool DamageComponent::IsAlive()
{
    auto h = GetHealth();
    return h > 0 && h <= 100;
}

FMinimalViewInfo CameraComponent::GetInfo()
{
    return Read<FMinimalViewInfo>((QWORD)this + Offsets::CameraCachePrivate);
}