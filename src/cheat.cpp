#include "headers/pch.h"

Cheat* cheat = nullptr;

Cheat::Cheat()
{
}

Cheat::~Cheat()
{
}

bool Cheat::Load()
{
   while(!kernel->AttachToProcess(_process_name))
        kernel->Sleep(1000);

    do
    {
        kernel->Sleep(1000);
        Memory::base_address = kernel->GetModuleHandle(_process_name);
    } while (!Memory::base_address);
    
    do
    {
        kernel->Sleep(1000);
        Memory::guarded_pool_address = kernel->GetGuardedPool();
    } while (!Memory::guarded_pool_address);
    
    
    print("Loaded - Base address: %p - Guarded pool: %p", Memory::base_address, Memory::guarded_pool_address);
    
    _check_process = std::thread([this]() {this->CheckProcess();});
    _check_process.detach();

    print("Started CheckProcess() thread");

    return true;
}

bool Cheat::CanUpdate()
{
    return _process_exists;
}

void Cheat::Update()
{
    if(Engine::Update())
    {
        kernel->Sleep(1500);
        return;
    }

    if(Aimbot::Update())
    {
        kernel->Sleep(1500);
        return;
    }
}

bool Cheat::Unload()
{
    return true;
}

void Cheat::CheckProcess()
{
    _process_exists = true;
    while(true)
    {
        if(!kernel->GetProcessID(_process_name))
        break;

        kernel->Sleep(50);
    }

    print("Process NOT found!");
    _process_exists = false;
}
