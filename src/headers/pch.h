#pragma once
#include <iostream>
#include <vector>
#include <thread>
#include <cmath>
#include <immintrin.h>

typedef char                                CHAR;
typedef unsigned char                       BYTE, *PBYTE, *LPBYTE, UCHAR;
typedef unsigned short                      WORD, *PWORD;
typedef unsigned int                        DWORD, *PDWORD, *LPDWORD, BOOL, *PBOOL, NTSTATUS;
typedef long long unsigned int              QWORD, *PQWORD, ULONG64, *PULONG64;
typedef void                                VOID, *PVOID, *HANDLE, **PHANDLE, *HMODULE;
typedef size_t                              SIZE_T, *PSIZE_T;

#include "kernel.h"
#include "memory.h"

#include "offsets.h"

#include "sdk.h"
#include "components.h"
#include "entity.h"
#include "engine.h"
#include "aimbot.h"

#include "plugin.h"
#include "cheat.h"