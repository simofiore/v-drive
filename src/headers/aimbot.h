#pragma once
#include "pch.h"

struct KillPacket
{
    FMinimalViewInfo camera_info = FMinimalViewInfo();
    Vector3 local_rotation = Vector3();

    Entity* target = nullptr;
    Vector3 head_angle = Vector3();
    float head_radius = 0;
    float distance_to_crosshair = 15.f;
};

class Aimbot
{

    static Entity* _last_target;
    static QWORD _last_target_time;

    static bool Kill(KillPacket* packet);
public:

    static bool Update();
};
