#pragma once
#include "pch.h"

class BaseEntity
{
protected:

    PawnComponent* _pawn = nullptr;
    DamageComponent* _damage = nullptr;

public:

    BaseEntity() : _pawn(nullptr), _damage(nullptr) {}
    BaseEntity(QWORD pawn, QWORD damage) : _pawn((PawnComponent*)pawn), _damage((DamageComponent*)damage) {}
    ~BaseEntity() {}
    
    PawnComponent* GetPawn() {return _pawn;}
    DamageComponent* GetDamageComponent() {return _damage;}

    bool IsValid() { return _pawn && _damage;}
};

class LocalPlayer : public BaseEntity
{
    ControllerComponent* _controller = nullptr;
    InventoryComponent* _inventory = nullptr;

public:

    LocalPlayer()
    {
        BaseEntity::_pawn = nullptr;
        BaseEntity::_damage = nullptr;
        _controller = nullptr;
        _inventory = nullptr;
    }
    LocalPlayer(QWORD pawn,QWORD damage,QWORD controller,QWORD inventory)
    {
        BaseEntity::_pawn = (PawnComponent*)pawn;
        BaseEntity::_damage = (DamageComponent*)damage;
        _controller = (ControllerComponent*)controller;
        _inventory = (InventoryComponent*)inventory;
    }
    ~LocalPlayer() {}

    ControllerComponent* GetControllerComponent() {return _controller;}
    InventoryComponent* GetInventoryComponent() {return _inventory;}

    bool IsValid()
    {
        if (!_controller or !_inventory) return false;
        else return BaseEntity::IsValid();
    }

};

class Entity : public BaseEntity
{
    RootComponent* _root = nullptr;
    MeshWrapper _mesh_wrapper = MeshWrapper();

    //Aim location
    Vector3 _last_targeting_position = Vector3();
    Vector3 _last_velocity = Vector3();
    QWORD _last_tick = 0;

public:

    Entity() 
    {
        BaseEntity::_pawn = nullptr;
        BaseEntity::_damage = nullptr;
        _root = nullptr;
        _mesh_wrapper = MeshWrapper();
        
        _last_targeting_position = Vector3();
        _last_velocity = Vector3();
        _last_tick = 0;

    }
    Entity(QWORD pawn, QWORD damage, QWORD root, QWORD mesh, TArray<FTransform> bone_array)
    {
        BaseEntity::_pawn = (PawnComponent*)pawn;
        BaseEntity::_damage = (DamageComponent*)damage;
        _root = (RootComponent*)root;
        _mesh_wrapper = MeshWrapper((MeshComponent*)mesh, bone_array);

        _last_targeting_position = Vector3();
        _last_velocity = Vector3();
        _last_tick = 0;

    }
    ~Entity(){}

    RootComponent* GetRootComponent() {return _root;}
    MeshWrapper* GetMeshWrapper(){return &_mesh_wrapper;}

    Vector3* GetLastTargetingPosition() { return &_last_targeting_position;}
    Vector3* GetLastVelocity() { return &_last_velocity;}
    QWORD* GetLastTick() { return &_last_tick;}

    bool IsValid()
    {
        if(!_root or !_mesh_wrapper.GetMeshComponent()) return false;
        else return BaseEntity::IsValid();
    }
};