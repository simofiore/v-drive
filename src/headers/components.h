#pragma once
#include "pch.h"

class PawnComponent
{
public:
    int member =0;

    bool IsDormant();
};

class ControllerComponent
{
public:

    Vector3 GetRotation();
};

class RootComponent
{
public:

    Vector3 GetVelocity();
    Vector3 GetPosition();
};

class MeshComponent
{
    //TArray<FTransform> GetBoneArray();
    //TArray<FTransform> _bone_array;
public:

    bool IsRendered();

};

class MeshWrapper
{
    MeshComponent* _mesh = nullptr;
    TArray<FTransform> _bone_array = TArray<FTransform>();

public:

    enum BONEID : DWORD
    {
        HEAD = 8,
        CHEST = 6
    };
    Vector3 GetBonePosition(BONEID id);
    MeshComponent* GetMeshComponent() { return _mesh;}

    MeshWrapper() {}
    MeshWrapper(MeshComponent* mesh, TArray<FTransform> array) : _mesh(mesh), _bone_array(array) {}
    ~MeshWrapper() {}
};

class Equipped
{
public:
    enum STATE : DWORD
    {
        UNKNOWN = 0,
        IDLE = 1,
        EQUIPPING =2,
        INSPECTING =3,
        ATTACKING = 4,
        FIRING = 5,
        RELOADING = 6,
        ADS_IDLE = 7,
        ADS_FIRING = 8,
        COUNT = 9,
        EAREQUIPPABLESTATE_MAX = 10
    };

    STATE GetState();
    bool CanShoot();
};

class InventoryComponent
{
public:

    Equipped* GetCurrentEquipped();

};

class DamageComponent
{
public:

    float GetHealth();
    bool IsAlive();
};

class CameraComponent
{
public:

    FMinimalViewInfo GetInfo();
};

/*clss methos, for some reason defining it in the .cpp file break everything*/

/*

bool PawnComponent::IsDormant()
{
    return Read<int>((QWORD)this + Offsets::Dormant) == 1;
}

Vector3 ControllerComponent::GetRotation()
{
    return Read<Vector3>((QWORD)this + Offsets::ControlRotation);
}

Vector3 RootComponent::GetVelocity()
{
    return Read<Vector3>((QWORD)this + Offsets::RootVelocity);
}

Vector3 RootComponent::GetPosition()
{
    return Read<Vector3>((QWORD)this + Offsets::RootPosition);
}

TArray<FTransform> MeshComponent::GetBoneArray()
{
    return Read<TArray<FTransform>>((QWORD)this + Offsets::BoneTArray);
}

bool MeshComponent::IsRendered()
{
    float l_r_t = Read<float>((QWORD)this + Offsets::LastRenderTime);
    float l_s_t = Read<float>((QWORD)this + Offsets::LastSubmitTime);
    
    bool is_visible = l_r_t + 0.06f >= l_s_t;
    return is_visible;
}

Vector3 MeshComponent::GetBonePosition(MeshComponent::BONEID id)
{
    auto bone = GetBoneArray()[id];
    auto component_to_world = Read<FTransform>((QWORD)this + Offsets::ComponentToWorld);
    MATRIX mtx = bone.ToMatrixWithScale().Multiply(component_to_world.ToMatrixWithScale());
    return Vector3(mtx._41, mtx._42, mtx._43);
}

Equipped::STATE Equipped::GetState()
{
    return Read<STATE>((QWORD)this + Offsets::CurrentEquippedVFXState);
}

bool Equipped::CanShoot()
{
    auto state = GetState();
    if(state == STATE::UNKNOWN || state == STATE::IDLE || state == STATE::FIRING) return true;
    else return false;
}

Equipped* InventoryComponent::GetCurrentEquipped()
{
    return Read<Equipped*>((QWORD)this + Offsets::CurrentEquipped);
}

float DamageComponent::GetHealth()
{
    return Read<float>((QWORD)this + Offsets::Health);
}

bool DamageComponent::IsAlive()
{
    auto h = GetHealth();
    return h > 0 && h <= 100;
}

FMinimalViewInfo CameraComponent::GetInfo()
{
    return Read<FMinimalViewInfo>((QWORD)this + Offsets::CameraCachePrivate);
}

*/