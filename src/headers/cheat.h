#pragma once
#include "pch.h"

class Cheat : private Plugin
{
public:

    Cheat();
    virtual ~Cheat();

    virtual bool Load();
    virtual bool CanUpdate();
    virtual void Update();
    virtual bool Unload();

private:

    static inline const std::string _process_name = "VALORANT-Win64-Shipping.exe";
    
    static bool inline _process_exists = false;
    static inline std::thread _check_process;
    void CheckProcess();


};

extern Cheat* cheat;
