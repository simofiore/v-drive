#pragma once
#include "pch.h"

namespace Engine
{
    namespace Cached{
        extern QWORD world;
        extern QWORD xor_keys;
        extern QWORD game_state;
        extern TArray<QWORD> player_state_array;
        extern CameraComponent* camera_component;
        extern LocalPlayer me;
        extern std::vector<Entity> entities;
    }

    const char* GetName(int id);
    bool Update();
}
