#pragma once
#include "pch.h"

#define LBUTTON         0
#define RBUTTON         1
#define MOUSE3          2
#define MOUSE4          3
#define MOUSE5          4

#define ARDUINO(x) x + 1
class Kernel
{
    private:

    Kernel() {};
    ~Kernel() {};

    public:

    virtual DWORD GetProcessID(const std::string& process_name) = 0;
    virtual BOOL AttachToProcess(const std::string& process_name) = 0;
    virtual QWORD GetModuleHandle(const std::string& module_name) = 0;
    virtual BOOL Read(QWORD address, QWORD buffer, SIZE_T size, BOOL physical = false)= 0;
    virtual BOOL ReadKernel(QWORD address, QWORD buffer, SIZE_T size)= 0;
    virtual QWORD GetGuardedPool() = 0;
    virtual VOID SetRawInput(bool enabled) = 0;
    virtual BOOL SendMouseMove(int x, int y, int wheel)= 0;
    virtual BOOL SendMouseInput(int key, bool released)= 0;
    virtual BOOL SendKeyboardInput(int key, bool released)= 0;
    virtual BOOL IsKeyPressed(int key) = 0;

    virtual VOID Sleep(int ms) = 0;
    virtual QWORD GetTickCount()=0;
    virtual VOID Print(const char* function, const char* msg, ...) = 0;

};

extern Kernel* kernel;

#define print(msg, ...) kernel->Print(__FUNCTION__ ,msg, ##__VA_ARGS__)


