#pragma once
#include "pch.h"

class Plugin
{
public:

    Plugin() {};
    virtual ~Plugin() {};

    virtual bool Load() = 0;
    virtual bool CanUpdate() = 0;
    virtual void Update() = 0;
    virtual bool Unload() = 0;

};