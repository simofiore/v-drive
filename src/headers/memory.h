#pragma once
#include "pch.h"

class Memory
{

public:

    static QWORD base_address;
    static QWORD guarded_pool_address;

    static bool ReadEx(QWORD address, QWORD buffer, size_t);

};

template <typename T>
static T Read(QWORD address)
{
    T result ={};
    if(Memory::ReadEx(address, (QWORD)&result, sizeof(T))) return result;
    else return {};
}