#pragma once
#include "pch.h"

#define M_PI                       3.14159265358979323846f

#define RADTODEG 180 / M_PI
#define DEGTORAD M_PI / 180 

#define RadianToDegree( Radian )			( ( Radian ) * ( 180.0f / M_PI ) )


typedef struct _MATRIX
{
	union {
		struct {
			float        _11, _12, _13, _14;
			float        _21, _22, _23, _24;
			float        _31, _32, _33, _34;
			float        _41, _42, _43, _44;

		};
		float m[4][4];
	};

	_MATRIX Multiply(_MATRIX a)
	{
			_MATRIX pOut;
	pOut._11 = _11 * a._11 + _12 * a._21 + _13 * a._31 + _14 * a._41;
	pOut._12 = _11 * a._12 + _12 * a._22 + _13 * a._32 + _14 * a._42;
	pOut._13 = _11 * a._13 + _12 * a._23 + _13 * a._33 + _14 * a._43;
	pOut._14 = _11 * a._14 + _12 * a._24 + _13 * a._34 + _14 * a._44;
	pOut._21 = _21 * a._11 + _22 * a._21 + _23 * a._31 + _24 * a._41;
	pOut._22 = _21 * a._12 + _22 * a._22 + _23 * a._32 + _24 * a._42;
	pOut._23 = _21 * a._13 + _22 * a._23 + _23 * a._33 + _24 * a._43;
	pOut._24 = _21 * a._14 + _22 * a._24 + _23 * a._34 + _24 * a._44;
	pOut._31 = _31 * a._11 + _32 * a._21 + _33 * a._31 + _34 * a._41;
	pOut._32 = _31 * a._12 + _32 * a._22 + _33 * a._32 + _34 * a._42;
	pOut._33 = _31 * a._13 + _32 * a._23 + _33 * a._33 + _34 * a._43;
	pOut._34 = _31 * a._14 + _32 * a._24 + _33 * a._34 + _34 * a._44;
	pOut._41 = _41 * a._11 + _42 * a._21 + _43 * a._31 + _44 * a._41;
	pOut._42 = _41 * a._12 + _42 * a._22 + _43 * a._32 + _44 * a._42;
	pOut._43 = _41 * a._13 + _42 * a._23 + _43 * a._33 + _44 * a._43;
	pOut._44 = _41 * a._14 + _42 * a._24 + _43 * a._34 + _44 * a._44;

	return pOut;

	}
} MATRIX;

class Vector3 {
public:
	Vector3() : x(0.f), y(0.f), z(0.f) {}
	Vector3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
	~Vector3() {}

	float x;
	float y;
	float z;

	inline float Dot(Vector3 v) {
		return x * v.x + y * v.y + z * v.z;
	}

	inline float Distance(Vector3 v) {
		return float(sqrtf(powf(v.x - x, 2.0) + powf(v.y - y, 2.0) + powf(v.z - z, 2.0)));
	}

	Vector3 operator+(Vector3 v) {
		return Vector3(x + v.x, y + v.y, z + v.z);
	}

	Vector3 operator-(Vector3 v) {
		return Vector3(x - v.x, y - v.y, z - v.z);
	}

	Vector3 operator*(float v) {
		return Vector3(x * v, y * v, z * v);
	}

	Vector3 operator/(float v) {
		return Vector3(x / v, y / v, z / v);
	}

	Vector3 operator+=(Vector3 v) {

		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	Vector3 operator-=(Vector3 v) {

		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	Vector3 operator*=(Vector3 v) {

		x *= v.x;
		y *= v.y;
		z *= v.z;
		return *this;
	}

	Vector3 operator/=(Vector3 v) {

		x /= v.x;
		y /= v.y;
		z /= v.z;
		return *this;
	}

	bool operator==(Vector3 v) {

		return x == v.x and y == v.y and z == v.z;
	}

	inline float Length() {
		return sqrtf((x * x) + (y * y) + (z * z));
	}

};

struct FQuat {
	float x;
	float y;
	float z;
	float w;
};

struct FTransform {
	FQuat rot;
	Vector3 translation;
	char pad[4];
	Vector3 scale;
	char pad1[4];
	MATRIX ToMatrixWithScale() {
		MATRIX m;
		m._41 = translation.x;
		m._42 = translation.y;
		m._43 = translation.z;

		float x2 = rot.x + rot.x;
		float y2 = rot.y + rot.y;
		float z2 = rot.z + rot.z;

		float xx2 = rot.x * x2;
		float yy2 = rot.y * y2;
		float zz2 = rot.z * z2;
		m._11 = (1.0f - (yy2 + zz2)) * scale.x;
		m._22 = (1.0f - (xx2 + zz2)) * scale.y;
		m._33 = (1.0f - (xx2 + yy2)) * scale.z;

		float yz2 = rot.y * z2;
		float wx2 = rot.w * x2;
		m._32 = (yz2 - wx2) * scale.z;
		m._23 = (yz2 + wx2) * scale.y;

		float xy2 = rot.x * y2;
		float wz2 = rot.w * z2;
		m._21 = (xy2 - wz2) * scale.y;
		m._12 = (xy2 + wz2) * scale.x;

		float xz2 = rot.x * z2;
		float wy2 = rot.w * y2;
		m._31 = (xz2 + wy2) * scale.z;
		m._13 = (xz2 - wy2) * scale.x;

		m._14 = 0.0f;
		m._24 = 0.0f;
		m._34 = 0.0f;
		m._44 = 1.0f;

		return m;
	}
};

class FRotator
{
public:
	float Pitch = 0.f;
	float Yaw = 0.f;
	float Roll = 0.f;

	MATRIX GetAxes() {
		return Matrix();
	}

	MATRIX Matrix(Vector3 origin = Vector3(0, 0, 0)) {
		float radPitch = (Pitch * float(M_PI) / 180.f);
		float radYaw = (Yaw * float(M_PI) / 180.f);
		float radRoll = (Roll * float(M_PI) / 180.f);
		float SP = sinf(radPitch);
		float CP = cosf(radPitch);
		float SY = sinf(radYaw);
		float CY = cosf(radYaw);
		float SR = sinf(radRoll);
		float CR = cosf(radRoll);

		MATRIX matrix;
		matrix.m[0][0] = CP * CY;
		matrix.m[0][1] = CP * SY;
		matrix.m[0][2] = SP;
		matrix.m[0][3] = 0.f;

		matrix.m[1][0] = SR * SP * CY - CR * SY;
		matrix.m[1][1] = SR * SP * SY + CR * CY;
		matrix.m[1][2] = -SR * CP;
		matrix.m[1][3] = 0.f;

		matrix.m[2][0] = -(CR * SP * CY + SR * SY);
		matrix.m[2][1] = CY * SR - CR * SP * SY;
		matrix.m[2][2] = CR * CP;
		matrix.m[2][3] = 0.f;

		matrix.m[3][0] = origin.x;
		matrix.m[3][1] = origin.y;
		matrix.m[3][2] = origin.z;
		matrix.m[3][3] = 1.f;

		return matrix;
	}
};

struct FMinimalViewInfo
{
	Vector3 Location; //+ 0x1260
	FRotator Rotation; //+ 0x126C
	float FOV;     //+ 0x1278
};

template<class T>
class TArray
{

    public:

    T* data;
    DWORD count,max_count;

    T operator[](DWORD i) const
    { return Read<T>((QWORD)data + i *sizeof(T)); };

};

template<class T> inline bool operator == (const TArray<T>& a, const TArray<T>& b)
{
    if(a.data == b.data && a.count == b.count) return true;
    else return false;

}

template<class T> inline bool operator != (const TArray<T>& a, const TArray<T>& b)
{
    if(a.data != b.data || a.count != b.count) return true;
    else return false;

}

struct FNameEntryHandle
{
    WORD is_wide : 1;
    WORD len : 15;
};

struct FNameEntry
{
    DWORD comparison_id;
    FNameEntryHandle header;
	char    ansi_name[1024];

    char const* GetAnsiName() const {return ansi_name;}
    bool IsWide() const {return header.is_wide;}
    
};

enum class TEAMID : BYTE
{
    ATTACKER = 0,
    DEFENDER,
};

// class PawnComponent
// {
// public:
//     int member =0;

//     bool IsDormant();
// };