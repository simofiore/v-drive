#pragma once
#include "pch.h"

namespace Offsets
{
    static constexpr QWORD World = 0x60;
    static constexpr QWORD XorKeys = 0x0;
    static constexpr QWORD FNamePool = 0xa0fa540;

    //2 retreive all networking actors
    static constexpr QWORD GameState = 0x140;//from world AGameStateBase 
    static constexpr QWORD NetworkablePlayerStateTArray = 0x3f0;//from gamestate /0x3e8

    //2 retreive local player
    static constexpr QWORD GameInstance = 0x1a0;//from world
    static constexpr QWORD LocalPlayerArray = 0x40;//from gameinstance
    static constexpr QWORD LocalPlayer = 0x0;//first in array
    static constexpr QWORD LocalPlayerController= 0x38;//from whatever that shit is
    static constexpr QWORD ControlRotation= 0x448;//from controller
    static constexpr QWORD LocalPlayerPawn = 0x468;//from controller //0x460
    static constexpr QWORD ActorID = 0x18;//from pawn
    static constexpr QWORD UniqueID = 0x38;//from pawn
    static constexpr QWORD Dormant = 0x102;//from pawn

    static constexpr QWORD RootComponent = 0x238; //from pawn //0x230 VALIDO
    static constexpr QWORD RootPosition = 0x164; //from component
    static constexpr QWORD RootRotation = 0x170; //from component
    //static constexpr QWORD RootScale = 0x17c; //from component
    static constexpr QWORD RootVelocity = 0x188; //from component
    //static constexpr QWORD RootVisible = 0x194; //from component
 
    static constexpr QWORD PlayerState = 0x3f8; //from pawn //0x3f0
    static constexpr QWORD PawnSpawnedCharacter = 0x928;//from plauyerstate //VALIDO  - struct AShooterCharacter* SpawnedCharactr; from ShooterGame_classes.hs
    static constexpr QWORD TeamComponent = 0x630; //from playerstate //VALIDO
    static constexpr QWORD TeamID = 0xf8;//from component

    static constexpr QWORD MeshComponent = 0x438;//from pawn // 0x430
    static constexpr QWORD ComponentToWorld = 0x250;//from component
    static constexpr QWORD BoneTArray = 0x5c8;//from component
    static constexpr QWORD LastSubmitTime = 0x380;//from component
    static constexpr QWORD LastRenderTime = LastSubmitTime + 0x4;//from component

    static constexpr QWORD InventoryComponent = 0x9b0; //from pawn //0x9a0 VALIDO
    static constexpr QWORD CurrentEquipped = 0x248; //from component
    static constexpr QWORD CurrentEquippedVFXState = 0xcf8; // from current equippable VALIDO

    static constexpr QWORD DamageComponent = 0xa10; //from pawn //0xa00
    static constexpr QWORD Health = 0x1b0;//from component

    static constexpr QWORD CameraComponent = 0x480; //from controller //0x478
    static constexpr QWORD CameraCache = 0x1230 + 0x10; //from component 
    static constexpr QWORD CameraCachePrivate = 0x1FF0; //from component 

}