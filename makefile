CXX = clang++
CXXFLAGS = -std=c++17 -Wno-macro-redefined -Wall -fPIC -I src/headers #-Wno-c++20-extensions 
LIBS = -shared
SRC_DIR = src
BIN_DIR = bin
SOURCES = $(wildcard $(SRC_DIR)/*.cpp)
OBJECTS = $(SOURCES:$(SRC_DIR)/%.cpp=$(BIN_DIR)/%.o)
SO_TARGET = $(BIN_DIR)/valorant.so

.PHONY: all clean

all: $(SO_TARGET)

$(SO_TARGET): $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(LIBS) -o $@ $^

$(BIN_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -f $(BIN_DIR)/*.o $(SO_TARGET)
